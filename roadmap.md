# Backlog

- Afficher un graphique illustrant le taux moyen / jour des 10 derniers jours
- Afficher l'historique de la variation Euro/ Usd par mois sur un an tout en affichant le pourcentage négatif en rouge et le positif en vert
- Avoir des listes déroulantes pour afficher les devises de départ et la devise cible
