import { Component, OnInit } from '@angular/core';

interface DeviseHistory {
  realExchangeRate: number;
  forcedExchangeRate: number;
  initValue: number;
  initDevise: string;
  calculatedValue: number;
  targetDevise: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  realExchangeRate: number = 1.1;
  forcedExchangeRate: number = 0;
  epsilon: number = 0;
  euroValue: number = 1;
  usdValue: number = 0;
  force: boolean = false;
  euroToUsd: boolean = true;
  variation: number = 0;
  histories: Array<DeviseHistory> = new Array<DeviseHistory>();

  constructor() {}

  ngOnInit(): void {
    this.usdValue = this.realExchangeRate * this.euroValue;
    setInterval(() => {
      // Réponse question: 7
      /*if (this.force && this.forcedExchangeRate > 0) {
        this.variation =
          ((this.realExchangeRate - this.forcedExchangeRate) /
            this.realExchangeRate) *
          100;

        if (Math.abs(this.variation) > 2) {
          this.force = false;
          this.forcedExchangeRate = 0;
          this.variation = 0;
        }
      }*/

      this.realExchangeRate = this.realExchangeRate + this.randomInt();
    }, 3000);
  }

  randomInt() {
    const max = 0.05;
    const min = -0.05;

    return Math.random() * (max - min) + min;
  }

  euroValuechange(event: number) {
    if (event == null) {
      return;
    }
    this.euroValue = event;
  }

  exchangeEuroToUsd() {
    const rate =
      this.force && this.forcedExchangeRate > 0
        ? this.forcedExchangeRate
        : this.realExchangeRate;
    this.usdValue = rate * this.euroValue;
    this.addNewHistory('EURO', 'USD');
  }

  usdValuechange(event: number) {
    if (event == null) {
      return;
    }
    this.usdValue = event;
  }

  exchangeUsdToEuro() {
    const rate =
      this.force && this.forcedExchangeRate > 0
        ? this.forcedExchangeRate
        : this.realExchangeRate;
    this.euroValue = this.usdValue / rate;
    this.addNewHistory('USD', 'EURO');
  }

  forcedExchangeRateValuechange(event: number) {
    this.forcedExchangeRate = 0;
    if (event == null) {
      return;
    }
    if (event > 0) {
      this.forcedExchangeRate = event;
    }
  }

  exchangeWithForcedExchangeRate() {
    if (this.forcedExchangeRate > 0) {
      if (this.euroToUsd) {
        this.usdValue = this.forcedExchangeRate * this.euroValue;
        this.addNewHistory('EURO', 'USD');
      } else {
        this.euroValue = this.usdValue / this.forcedExchangeRate;
        this.addNewHistory('USD', 'EURO');
      }
    }
  }

  addNewHistory(initDevise: string, targetDevise: string) {
    const history: DeviseHistory = {
      realExchangeRate: this.realExchangeRate,
      forcedExchangeRate: this.forcedExchangeRate,
      initValue: this.euroValue,
      initDevise,
      calculatedValue: this.usdValue,
      targetDevise,
    };

    if (this.histories.length === 5) {
      this.histories.shift();
    }
    this.histories.push(history);
  }
}
